package com.nareshittechnologies.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonService {

    @GET("posts")
    Call<List<Post>> getData();
}
