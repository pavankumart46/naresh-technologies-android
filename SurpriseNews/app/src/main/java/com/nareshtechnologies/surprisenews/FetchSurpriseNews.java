package com.nareshtechnologies.surprisenews;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.Buffer;

import javax.net.ssl.HttpsURLConnection;

public class FetchSurpriseNews extends AsyncTask<String,Void,String> {

    Context context;
    RecyclerView recyclerView;
    ProgressBar progress;

    public FetchSurpriseNews(Context context, RecyclerView recyclerView,ProgressBar progress) {
        this.context = context;
        this.recyclerView = recyclerView;
        this.progress = progress;
    }

    @Override
    protected String doInBackground(String... strings) {
        // Make the url into a proper url
       // TODO 1: Convert the string type url to URL object
        try {
            URL url = new URL(strings[0]);
            // TODO 2: Open HttpsURLConnection - as the url starts with https://
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            // TODO 3: get the inputStream out of the connection to read the data that is coming in
            InputStream is = connection.getInputStream();
            // TODO 4: To Create a BufferedReader object that will aid us in reading the data
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            // TODO 5: With the help of the BufferedReader object - Read the actual data
            StringBuilder builder = new StringBuilder();
            String input = "";
            while((input = br.readLine())!=null){
                builder.append(input);
            }
            return builder.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progress.setVisibility(View.INVISIBLE);
        /*try {
            JSONObject root = new JSONObject(s);
            JSONArray data = root.getJSONArray("data");
            result.setText("");
            for(int i=0; i<data.length(); i++){
                JSONObject item = data.getJSONObject(i);
                String title = item.getString("title");
                String content = item.getString("content");
                String author = item.getString("author");
                String articleUrl = item.getString("url");
                String date = item.getString("date");

                result.append(title+"\n\n");
                result.append(content+"\n\n");
                result.append(author+"\n\n");
                result.append(date+"\n\n");
                result.append(articleUrl+"\n\n");
                result.append("----------\n\n\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        Gson gson = new Gson();
        Root root = gson.fromJson(s,Root.class);
        NewsArticlesAdapter naa = new NewsArticlesAdapter(context,root.getData());
        recyclerView.setAdapter(naa);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
    }
}
