package com.nareshtechnologies.surprisenews;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    Spinner s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.recyclerview);
        progressBar.setVisibility(View.INVISIBLE);

        /*s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String category = adapterView.getItemAtPosition(i).toString();
                String url = "https://inshortsapi.vercel.app/news?category="+category;
                fetchSurprises(url);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(MainActivity.this, "NO CATEGORY IS SELECTED", Toast.LENGTH_SHORT).show();
            }
        });*/
        Intent i = getIntent();
        String url = i.getStringExtra("URL_KEY");
        fetchSurprises(url);
    }

    // This method will fetch the surprise news from
    // across the globe
    public void fetchSurprises(String url) {
        progressBar.setVisibility(View.VISIBLE);
        FetchSurpriseNews surpriseNews = new FetchSurpriseNews(this,recyclerView,progressBar);
        surpriseNews.execute(url);
    }
}