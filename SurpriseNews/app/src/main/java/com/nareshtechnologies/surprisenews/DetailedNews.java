package com.nareshtechnologies.surprisenews;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class DetailedNews extends AppCompatActivity {

    ImageView detailed_news_image;
    TextView detailed_news_title, detialed_news_content;
    Datum d;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_news);
        detailed_news_image = findViewById(R.id.detailed_news_image);
        detailed_news_title = findViewById(R.id.detailed_news_title);
        detialed_news_content = findViewById(R.id.detailed_news_content);

        Intent i = getIntent();
        d = (Datum) i.getSerializableExtra("NARESH");
        Glide.with(this).load(d.getImageUrl()).into(detailed_news_image);
        detailed_news_title.setText(d.getTitle());
        detialed_news_content.setText(d.getContent());
    }

    public void showOriginalArticle(View view) {
        if(d.getUrl()!=null){
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(d.getUrl()));
            startActivity(i);
        }else{
            Toast.makeText(this, "Data you are looking for is not available", Toast.LENGTH_SHORT).show();
        }
    }

    public void readMore(View view) {
        if(d.getReadMoreUrl()!=null){
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse((String) d.getReadMoreUrl()));
            startActivity(i);
        }else {
            Toast.makeText(this, "Data you are looking for is not available", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}