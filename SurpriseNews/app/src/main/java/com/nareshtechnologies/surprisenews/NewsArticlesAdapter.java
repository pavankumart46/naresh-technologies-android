package com.nareshtechnologies.surprisenews;

import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class NewsArticlesAdapter extends RecyclerView.Adapter<NewsArticlesAdapter.NewsArticlesViewHolder> {

    Context context;
    List<Datum> articles;

    public NewsArticlesAdapter(Context context, List<Datum> articles) {
        this.context = context;
        this.articles = articles;
    }

    @NonNull
    @Override
    public NewsArticlesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.news_articles_design,parent,false);
        return new NewsArticlesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsArticlesViewHolder holder, int position) {
        Glide.with(context).load(articles.get(position).getImageUrl()).into(holder.article_image);
        holder.title.setText(articles.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public class NewsArticlesViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView article_image;
        public NewsArticlesViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.news_title);
            article_image = itemView.findViewById(R.id.imageView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Datum d = articles.get(position);
                    Intent i = new Intent(context,DetailedNews.class);
                    i.putExtra("NARESH",d);
                    context.startActivity(i);
                }
            });
        }
    }
}
