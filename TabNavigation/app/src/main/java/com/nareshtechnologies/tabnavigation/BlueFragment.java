package com.nareshtechnologies.tabnavigation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class BlueFragment extends Fragment implements View.OnClickListener {

    int count = 0;
    TextView res;
    public BlueFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_blue, container, false);
        Button p = v.findViewById(R.id.plusBtn);
        Button m = v.findViewById(R.id.minusBtn);
        res = v.findViewById(R.id.result);
        p.setOnClickListener(this);
        m.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.plusBtn){
            count++;
        }else{
            count--;
        }
        res.setText(String.valueOf(count));
    }
}