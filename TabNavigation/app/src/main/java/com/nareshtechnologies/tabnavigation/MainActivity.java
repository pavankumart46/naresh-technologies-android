package com.nareshtechnologies.tabnavigation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    ViewPager vp;
    TabLayout tb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vp = findViewById(R.id.viewpager);
        tb = findViewById(R.id.tabLayout);

        vp.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        tb.setupWithViewPager(vp);
    }

    // Creating an Adapter for ViewPager
    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(@NonNull FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0: return new RedFragment();

                case 1: return new BlueFragment();

                case 2: return new GreenFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0: return "RED";
                case 1: return "BLUE";
                case 2: return "GREEN";
            }
            return super.getPageTitle(position);
        }
    }

    /**
     * For TabNavigation there are two important components that you need to understand
     * - ViewPager
     *         - For TabNavigation, we will be working on Multiple Screens. ViewPager is a
     *         Component that hosts all these Screens side by side. It will also provide a
     *         feature to swipe through the screens
     * - TabLayout
     *         - TabLayout hosts the Tab Names, clicking on which takes us to the appropriate screen
     *         */
}