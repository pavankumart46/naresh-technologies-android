package com.nareshittechnologies.currencyconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private EditText value;
    private Spinner from,to;
    private TextView result;
    private String from_currency, to_currency;
    RequestQueue request;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        value = findViewById(R.id.amount);
        from = findViewById(R.id.from_spinner);
        to = findViewById(R.id.to_spinner);
        result = findViewById(R.id.result);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);

        from.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                from_currency = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        to.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                to_currency = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        request = Volley.newRequestQueue(this);
    }

    public void convertCurrency(View view) {
        progressBar.setVisibility(View.VISIBLE);
        String val = value.getText().toString();
        String url = "https://api.frankfurter.app/latest?amount="+val+"&from="+from_currency+"&to="+to_currency+"";
        Log.v("MAIN",url);
        String url2 = "https://api.frankfurter.app/latest?amount=1000&from=INR&to=USD";
        StringRequest r = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.INVISIBLE);
                // if url is hit
                try {
                    JSONObject root = new JSONObject(response);
                    JSONObject rates = root.getJSONObject("rates");
                    double r = rates.getDouble(to_currency);
                    result.setText(r+" "+to_currency);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // if there is an error fetching data
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        request.add(r);
    }
}