package com.nareshittechnologies.externalstorage;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    boolean isAvailable = false;
    boolean isWritable = false;
    boolean isReadable = false;
    EditText dataEditText;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataEditText = findViewById(R.id.data_et);
        result = findViewById(R.id.textView);

        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)){
            isAvailable = true;
            isWritable = true;
            isReadable = true;
        }else if(Environment.MEDIA_MOUNTED_READ_ONLY.equalsIgnoreCase(state)){
            isAvailable = true;
            isWritable = false;
            isReadable = true;
        }

        // TODO 1: First check if the permissions are already Granted
        if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED){
            Toast.makeText(this, "READ EXTERNAL STORAGE is granted", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "READ EXTERNAL STORAGE is Not Granted", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},123);
        }

        if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED){
            Toast.makeText(this, "WRITE EXTERNAL STORAGE is granted", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "WRITE EXTERNAL STORAGE is Not Granted", Toast.LENGTH_SHORT).show();
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},123);
        }

    }

    public void saveData(View view) {
        if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED){
            // You can write data to the file
            String data = dataEditText.getText().toString();

            File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

            // store data after creating the file
            File file = new File(folder,"external_storage.txt");
            FileOutputStream fos;
            try{
               fos = new FileOutputStream(file);
               fos.write(data.getBytes());
               fos.close();
            }catch (Exception e){

            }
        }else{
            Toast.makeText(this, "Data cannot be stored as permission is not granted!", Toast.LENGTH_SHORT).show();
        }
    }


    public void readData(View view) {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        // store data after creating the file
        File file = new File(folder,"external_storage.txt");

        FileInputStream fis;
        try{
            fis = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            StringBuilder sb = new StringBuilder();
            String line;
            while((line=br.readLine())!=null){
                sb.append(line);
            }
            result.setText(sb.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}