## Date: 03-01-2022
### [Introduction to Android - Presentation](https://docs.google.com/presentation/d/1U0gU4AJi5bqC8q0zij6Hx085mFag3SK48XLe9RvSsu8/edit?usp=sharing)
#### Concepts Covered
* What is android?
* Introduction to Mobile App Development
* Android Ecosystem
* Android Features
* Android Version History
* Android Platform Architecture

## Date: 04-01-2022
### [Setting up the development environment - Presentation](https://docs.google.com/presentation/d/1SagvfF6CIaxQOrpsJrnyidHrNWMelBHoqgJZ-Log2JA/edit?usp=sharing)
#### Concepts Covered
* Installation of Android Studio
    * [Download Link](https://developer.android.com/studio) for android studio setup
    * [Installation Guide - Link](https://developer.android.com/studio/install) for all Operating systems
    * [How to set up an Android Virtual Device(AVD) - Link](https://developer.android.com/studio/run/managing-avds) after the installation of android studio
* The Installation Task is given to the participants of the course.

## Date: 05-01-2022
#### Concepts Covered
* [Creating the first app](https://developer.android.com/training/basics/firstapp) on Android studio
* [Connecting a Physical Device](https://developer.android.com/studio/run/device) to debug (run) android apps directly from android studio

## Date: 06-01-2022
#### Concepts Covered
* [Project Structure of Android](https://developer.android.com/studio/projects)

## Date: 01-02-2022
#### Concepts Covered
* [Saved Instance State](https://developer.android.com/topic/libraries/architecture/saving-states)
* [Menus in android](https://developer.android.com/guide/topics/ui/menus)
* [Using Options Menu](https://developer.android.com/guide/topics/ui/menus#xml)
* [Score Keeper - Partially Completed](https://bitbucket.org/pavankumart46/naresh-technologies-android/src/master/ScoreKeeper/app/src/)


### SQL COMMANDS
- [Visit this site](https://sqliteonline.com/)

 * SQLite is an open source relational database that is used to perform database
 * operations on android devices.
 * - Storing
 * - retrieving
 * - Modifying
 * It is embedded in android by default.
 * Main Classes
 *  - SQLiteDatabase
 *      - to Perform database operations
 *  - SQliteOpenHelper
 *      - used for database creation and version management
 * - CRUD
 *  - CREATE
 *  CREATE TABLE students_data(student_id integer primary KEY AUTOINCREMENT, student_name text, studentAge Integer);
 *  INSERT into students_data(student_name,studentage) values("Pavan","30");
 *  - READ
 *  SELECT * from students_data;
 *  SELECT student_name from students_data;
 *  SELECT student_name from students_data order by student_name ASC;
 *  SELECT * from students_data where student_id = 2;
 *  - UPDATE
 *  Update students_data SET studentage = 25 where studentage>20;
 *  - DELETE
 *  delete from students_data where studentage>22;
 *  delete from students_data;
 *
 *  Deletes the entire table.
 *  drop table students_data;
 
 ### Assignment (26-May-2022)
 - Try adding code for Update and delete commands on RoomDatabaseExample Application
