package com.nareshtechnologies.scorekeeper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private int count = 0;
    private TextView display;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_naresh);
        // Connecting the "display" TextView to the real View on the main_activity_naresh.xml
        // We use `findViewById()` to do that
        display = findViewById(R.id.result);
        Toast.makeText(this, "onCreate()", Toast.LENGTH_SHORT).show();
        Log.v("MAIN","onCreate()");
        if(savedInstanceState!=null && savedInstanceState.containsKey("COUNT")){
            count = savedInstanceState.getInt("COUNT");
            display.setText(String.valueOf(count));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("MAIN","onStart()");
        Toast.makeText(this, "onStart()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("MAIN","onResume()");
        Toast.makeText(this, "onResume()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("MAIN","onPause()");
        Toast.makeText(this, "onPause()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v("MAIN","onStop()");
        Toast.makeText(this, "onStop()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v("MAIN","onDestroy()");
        Toast.makeText(this, "onDestroy()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.v("MAIN","onRestart()");
        Toast.makeText(this, "onRestart()", Toast.LENGTH_SHORT).show();
    }

    public void plusScore(View view) {
        // This method will be invoked when the plus button is clicked
        count++;
        display.setText(String.valueOf(count));
    }

    public void minusScore(View view) {
        // This method will be invoked when the minus button is clicked
        count--;
        display.setText(String.valueOf(count));
    }

    // We encounter this method when there is a configuration change of an activity
    // is taking place. Here we can save the state of the UI. This saved state
    // can be used further to persist the results on the UI
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("COUNT",count);
    }

    // This method attaches/inflates/adds an options menu item file to the activity.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    // This method is invoked as soon as an option is selected on the options menu file
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case R.id.reset:
                count = 0;
                display.setText(String.valueOf(count));
                break;
            case R.id.first:
                Toast.makeText(this, "First Option is selected", Toast.LENGTH_SHORT).show();
                break;
            case R.id.second:
                Toast.makeText(this, "Second Option is selected", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}