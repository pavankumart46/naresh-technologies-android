package com.nareshittechnologies.scorekeeper_databinding;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


public class MyViewModel extends ViewModel {
    public MutableLiveData<Integer> count = new MutableLiveData<>();
    public MyViewModel(){
        count.setValue(0);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }

    public void increment(){
        count.setValue(count.getValue() + 1);
    }

    public void decrement(){
        count.setValue(count.getValue() - 1);
    }
}
