package com.nareshittechnologies.scorekeeper_databinding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.R.integer;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import com.nareshittechnologies.scorekeeper_databinding.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding dataBinding;
    MyViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.activity_main);*/
        dataBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        viewModel = new ViewModelProvider(this).get(MyViewModel.class);
        viewModel.count.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                dataBinding.result.setText(String.valueOf(viewModel.count.getValue()));
            }
        });
    }

    public void plusScore(View view) {
        viewModel.increment();
    }

    public void minusScore(View view) {
        viewModel.decrement();
    }
}