package com.nareshittechnologies.sqlitedatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "students";
    private static final int DATABASE_VERSION = 1;
    Context context;

    public static final String TABLE_NAME = "students";
    public static final String COL_0 = "stu_id";
    public static final String COL_1 = "stu_name";
    public static final String COL_2 = "stu_age";

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_STUDENTS_TABLE = "create table "+TABLE_NAME+"("+COL_0+" integer primary key AUTOINCREMENT, " +
                ""+COL_1+" text, " +
                ""+COL_2+" integer);";
        sqLiteDatabase.execSQL(CREATE_STUDENTS_TABLE);
        Toast.makeText(context, "TABLE CREATED", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void insertData(ContentValues cv){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert("students",null,cv);
        Toast.makeText(context, "INSERTED", Toast.LENGTH_SHORT).show();
    }

    public Cursor getData(){
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("Select * from "+TABLE_NAME,null);
    }

    public void updateDetails(ContentValues cv, int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_NAME,cv,COL_0+"="+id,null);
        Toast.makeText(context, "UPDATED", Toast.LENGTH_SHORT).show();
    }
    
    public void delete(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_NAME+" where "+COL_0+"="+id);
        Toast.makeText(context, "DELETE OPERATION IS A SUCCESS", Toast.LENGTH_SHORT).show();
    }
}
