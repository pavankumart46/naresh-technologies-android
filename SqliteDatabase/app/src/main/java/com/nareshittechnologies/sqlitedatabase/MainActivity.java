package com.nareshittechnologies.sqlitedatabase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText sName, sAge;
    TextView result;
    DatabaseHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sName = findViewById(R.id.student_name);
        sAge = findViewById(R.id.student_age);
        result = findViewById(R.id.textView);

        helper = new DatabaseHelper(this);

    }

    public void saveData(View view) {
        String n = sName.getText().toString();
        int a = Integer.parseInt(sAge.getText().toString());
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.COL_1,n);
        contentValues.put(DatabaseHelper.COL_2,a);
        helper.insertData(contentValues);
    }

    public void loadData(View view) {
        Cursor c = helper.getData();
        if(c!=null){
            result.setText("");
            c.moveToFirst();
            do{
                int s_id = c.getInt(0);
                String s_n = c.getString(1);
                int s_a = c.getInt(2);
                result.append(s_id+"\t"+s_n+"\t"+s_a+"\n");
            }while (c.moveToNext());
        }
    }

    public void updateDetails(View view)
    {
        int s_id = Integer.parseInt(((EditText)findViewById(R.id.update_details)).getText().toString());
        String s_n = sName.getText().toString();
        String s_a = sAge.getText().toString();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COL_1,s_n);
        cv.put(DatabaseHelper.COL_2,s_a);
        helper.updateDetails(cv, s_id);
    }

    public void deleteEntry(View view) {
        int s_id = Integer.parseInt(((EditText)findViewById(R.id.update_details)).getText().toString());
        helper.delete(s_id);
    }
}