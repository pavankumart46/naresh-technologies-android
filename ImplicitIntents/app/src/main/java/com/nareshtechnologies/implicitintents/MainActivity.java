package com.nareshtechnologies.implicitintents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText phone, web_url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        phone = findViewById(R.id.phone_number);
        web_url = findViewById(R.id.url_entry);
    }

    public void placeCall(View view) {
        String p = phone.getText().toString();
        Intent i = new Intent();
        // Completed 1: Specify the Action Type
        i.setAction(Intent.ACTION_DIAL);
        // Completed 2: Uri data has to be passed to the intent
        i.setData(Uri.parse("tel:"+p));
        startActivity(i);
    }

    public void openBrowser(View view) {
        // TODO 1: First fetch the url from edit text
        String u = web_url.getText().toString();
        // todo 2: Create an intent with action ACTION_VIEW
        Intent i = new Intent();
        i.setAction(Intent.ACTION_VIEW);
        // TODO 3: Set the data (URI) using Uri.parse()
        i.setData(Uri.parse("https://"+u));
        // TODO 4: startActivity(...)
        startActivity(i);
    }

    public void openMaps(View view) {
        EditText e = findViewById(R.id.map_address);
        String add = e.getText().toString();
        Intent i = new Intent();
        i.setAction(Intent.ACTION_VIEW);
        i.setData(Uri.parse("geo:0,0?q="+add));
        startActivity(i);
    }
}