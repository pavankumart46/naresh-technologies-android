package com.nareshittechnologies.notifications;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    NotificationManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    public void sendNotification(View view) {
        // Create a Notification
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            // you need a NotificationChannel to be created
            NotificationChannel channel = new NotificationChannel("NareshNotify","Naresh IT Channel", NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription("All Defualt notifications");
            manager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"NareshNotify");
        // 1. Small ICON
        // 2. Title
        // 3. Message
        builder.setSmallIcon(R.drawable.umbrella);
        builder.setContentTitle("SAMPLE NOTIFICATION");
        builder.setContentText("Some sample text goes here");

        Intent i = new Intent(this, MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this,12,i,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pi);

        builder.setAutoCancel(true);

        builder.addAction(R.drawable.umbrella,"Reply",pi);

        // Big Picture Style - Example - Youtube Notification
        /*Bitmap b = BitmapFactory.decodeResource(getResources(),R.drawable.big_pic);
        builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(b));*/
        // Big Text Style - Example - GMAIL
        String bt = getResources().getString(R.string.big_text);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(bt));

        manager.notify(42,builder.build());

    }

    public void cancelNotification(View view) {
        manager.cancel(42);
    }
}