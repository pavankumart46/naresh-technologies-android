package com.nareshittechnologies.internalstorageex;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    EditText fileName, fileData;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fileName = findViewById(R.id.editText1);
        fileData = findViewById(R.id.editText2);
        result = findViewById(R.id.textView);
    }

    //TODO 2: Read Data from the file
    public void readData(View view) {
        String fn = fileName.getText().toString();
        FileInputStream fin;
        try {
            fin = openFileInput(fn+".txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fin));
            String line;
            StringBuilder sb = new StringBuilder();
            // Read the data line by line and store it in the line variable
            while((line = br.readLine())!=null){
                sb.append(line);
            }
            result.setText(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //TODO 1: Create a File and Write data to it
    public void writeData(View view) {
        String fn = fileName.getText().toString();
        String fd = fileData.getText().toString();

        FileOutputStream fos;

        try {
            fos = openFileOutput(fn+".txt",MODE_PRIVATE);
            fos.write(fd.getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Toast.makeText(this, "Data is stored successfully", Toast.LENGTH_SHORT).show();
    }
}

// Two Important Classes
/*
* FileInputStream
* FileOutputStream
* */