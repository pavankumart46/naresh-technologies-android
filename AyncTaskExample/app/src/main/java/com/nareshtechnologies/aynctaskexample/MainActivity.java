package com.nareshtechnologies.aynctaskexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void startTask(View view) {
        // This method is invoked as soon as start task button is pressed
        progressBar.setVisibility(View.VISIBLE);
        new AsyncHelper().execute();
    }

    class AsyncHelper extends AsyncTask<Void,Void,Void>{

        /**This is the mandatory method that we should override
         * What ever the background task that has to happen, it will
         * execute in the doInBackground Method
         * doInBackground(...) runs on Worker Thread.
         * */
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * onPostExecute() method processes the results and display them
         * on the UI.
         * onPostExecute Method runs on Main Thread or UI Thread*/
        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            progressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(MainActivity.this, "TASK IS COMPLETE!", Toast.LENGTH_SHORT).show();
        }
    }
}