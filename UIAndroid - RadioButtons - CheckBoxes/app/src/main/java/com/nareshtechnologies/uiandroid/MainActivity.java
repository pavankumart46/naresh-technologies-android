package com.nareshtechnologies.uiandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements AdapterView.OnItemSelectedListener, RadioGroup.OnCheckedChangeListener {

    private RadioGroup radioGroup;
    private CheckBox tel, eng, hin;
    private Spinner spinner;
    private String state;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        radioGroup = findViewById(R.id.rg_gender);
        tel = findViewById(R.id.telugu);
        eng = findViewById(R.id.english);
        hin = findViewById(R.id.hindi);
        spinner = findViewById(R.id.spinner);
        radioGroup.setOnCheckedChangeListener(this);
        spinner.setOnItemSelectedListener(this);
    }

    public void submitLanguages(View view) {
        String languages = "";
        if(tel.isChecked()){
            languages = languages+"Telugu\n";
        }
        if(eng.isChecked()){
            languages = languages+"English\n";
        }
        if(hin.isChecked()){
            languages += "Hindi\n"; // languages = languages+"Hindi\n";
        }
        Toast.makeText(this, languages+"\n"+state, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        state = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        state = parent.getItemAtPosition(0).toString();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
          switch (checkedId){
            case R.id.male:
                Toast.makeText(MainActivity.this, "MALE", Toast.LENGTH_SHORT).show();
                break;
            case R.id.female:
                Toast.makeText(MainActivity.this, "FEMALE", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}