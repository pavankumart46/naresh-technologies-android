package com.nareshittechnologies.jobschedulerex;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.AsyncTask;
import android.widget.Toast;

public class MyTaskJob extends JobService {
    JobParameters params;
    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        params = jobParameters;
        BackgroundWork bgw = new BackgroundWork();
        bgw.execute();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

    class BackgroundWork extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            jobFinished(params,true);
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            Toast.makeText(MyTaskJob.this, "JOB IS COMPLETE", Toast.LENGTH_SHORT).show();
        }
    }
}
