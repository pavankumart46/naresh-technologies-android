package com.nareshittechnologies.customreceiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.IntentFilter;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IntentFilter i = new IntentFilter();
        i.addAction(CustomReceiverBroadcast.Action);

        registerReceiver(new CustomReceiverBroadcast(),i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(new CustomReceiverBroadcast());
    }
}