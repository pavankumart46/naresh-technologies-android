package com.nareshittechnologies.customreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class CustomReceiverBroadcast extends BroadcastReceiver {

    public static final String Action = "com.nareshittechnologies.powerreceiver.ACTION_CUSTOM";
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if(intent.getAction() == Action){
            String d = intent.getStringExtra("INFO");
            Toast.makeText(context, "WE RECEIVED THE BROADCAST "+d, Toast.LENGTH_SHORT).show();
        }
    }
}