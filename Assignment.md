### Create an App That retrieves the related book items from google books api
[Click here to access the api and get json results](https://www.googleapis.com/books/v1/volumes?q={booktite/bookkind})
[Example Link](https://www.googleapis.com/books/v1/volumes?q=android)
The screen designs must go in the following order
[Refer the prototypes to get an understanding on the design](https://www.canva.com/design/DAE7Pql_ZW4/Fj4_AcxVuI3yS8KJa6LJkA/view?mode=prototype)
#### Screen 1:
- Google Sign in Button (You should use google sign in api to authenticate the user)
    - [Refer to the link to integrate Google Sign in](https://developers.google.com/identity/sign-in/android/start-integrating)
- Facebook Sign in Button (You should use facebook sign in api to authenticate the user)
    - [Refer to this link to integrate facebook singin to your app](https://developers.facebook.com/docs/facebook-login/android/)
- Other Authentication APIs can also be used
    - [Refer to this link to integrate any of other apis that you would like to integrate](https://github.com/public-apis/public-apis#social)

#### Screen 2:
- Present an EditText to allow the user enter his search query
- Also present a button to the user to allow him/her to click after entering the search result
- Display the details of the user on the top of the screen
    - The user details will be given by the APIs from first screen
- NOTE: Make sure that there is a search query entered into the edit text box when the button is clicked. If no data is found, throw an alert dialog box to the user

#### Screen 3,4:
- Follow the design and do it.

### DEADLINE for submission: 22nd March 2022
### How to submit your project ?
- Push the code to github/bitbucket under public repositories
- Also add few screenshots of the app that is developed.
- Write your name in the home Screen at the bottom end of the screen.
