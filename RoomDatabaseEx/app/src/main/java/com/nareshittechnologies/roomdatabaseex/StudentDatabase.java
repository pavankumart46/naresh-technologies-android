package com.nareshittechnologies.roomdatabaseex;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {StudentDetails.class},version = 1)
public abstract class StudentDatabase extends RoomDatabase {
    public abstract StudentDAO studentDAO();
}

/**
 * @Database(entities = {StudentDetails.class},version = 1) - defines the database and its version
 * read about it https://developer.android.com/training/data-storage/room */
