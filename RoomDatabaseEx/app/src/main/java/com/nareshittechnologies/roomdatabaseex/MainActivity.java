package com.nareshittechnologies.roomdatabaseex;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText stu_name, stu_age;
    TextView result;
    StudentDatabase database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        stu_name = findViewById(R.id.stu_name);
        stu_age = findViewById(R.id.stu_age);
        result = findViewById(R.id.result);

        database = Room.databaseBuilder(this,StudentDatabase.class,"student")
                .allowMainThreadQueries()
                .build();
    }

    public void saveData(View view) {
        String sN = stu_name.getText().toString();
        int sA = Integer.parseInt(stu_age.getText().toString());
        StudentDetails sd = new StudentDetails(sN,sA);
        database.studentDAO().insertData(sd);
        Toast.makeText(this, "Insertion is Successful", Toast.LENGTH_SHORT).show();
    }

    public void loadData(View view) {
        List<StudentDetails> data = database.studentDAO().getData();
        result.setText("");
        for(StudentDetails s:data){
            result.append(s.getStudent_id()+"\t"+s.getStudent_name()+"\t"+s.getStudent_age()+"\n");
        }
    }
}