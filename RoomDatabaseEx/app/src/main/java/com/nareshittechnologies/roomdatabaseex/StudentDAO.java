package com.nareshittechnologies.roomdatabaseex;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface StudentDAO {

    @Insert
    void insertData(StudentDetails studentDetails);

    @Query("SELECT * FROM student_details")
    List<StudentDetails> getData();
}

/**
 * @Dao - Makes an Interface as a Data Access Object
 * Read More - https://developer.android.com/training/data-storage/room/accessing-data
 * */