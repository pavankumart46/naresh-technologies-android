package com.nareshittechnologies.roomdatabaseex;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "student_details")
public class StudentDetails {
    @PrimaryKey(autoGenerate = true)
    int student_id;
    String student_name;
    int student_age;


    public StudentDetails(String student_name, int student_age) {
        this.student_name = student_name;
        this.student_age = student_age;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public int getStudent_age() {
        return student_age;
    }

    public void setStudent_age(int student_age) {
        this.student_age = student_age;
    }
}

/*
* In the entity class - the class Name becomes the table Name
* The fields will become the coloumn names
* All the entity classes must be annotated with @Entity
* To make a field (column) as Primary Key - Annotate it with @Primarykey
* if you want to increment the primary key up on every new insertion
* use autoGenerate = true inside the @PrimaryKey Annotation
* @Entity(tableName = "student_details") - will change the default table name
* @ColumnInfo(name = "stu_name") - Will change the default name of the coloumn
* */
