package com.nareshtechnologies.top10movies;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private int[] moviePosters;
    private String[] movieNames,movieDirectors;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerview);
        //Create data using the following method()
        createData();

        MoviesAdapter mAdapter = new MoviesAdapter(this,moviePosters,movieNames,
                movieDirectors);

        recyclerView.setAdapter(mAdapter);
       /* recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));*/
        /*recyclerView.setLayoutManager(new GridLayoutManager(this,2));*/
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
    }
    private void createData() {
        // Image - Movie poster - All the images that you use statically in the project will have an integer id in the background
        moviePosters = new int[]{R.drawable.a,R.drawable.b,R.drawable.c,R.drawable.d,R.drawable.e,R.drawable.f,
        R.drawable.g,R.drawable.h,R.drawable.i,R.drawable.j};
        movieNames = new String[]{"Albela","Bahubali","Chirutha","Daredevil","Edge of Tomorrow","Fana",
        "Gaurdians of Galaxy","Haider","I","Joker"};
        movieDirectors = new String[]{"Deepak Sareen","Rajamouli","Puri Jagannath","Mark Steven Johnson",
        "Doug Liman","Kunal Kohli","James Gunn","Vishal Bhardwaj","Shankar","Todd Phillips"};
        // String - Movie Name
        // String - Movie Director
    }

    // I Would like to override the default functionality of the back button

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        // if you write the code after the super class method call - it is of no use.
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Do you really want to exit the app?");
        alert.setMessage("You have been using this app from quite a long time. If you want to exit press yes otherwise press cancel");
        // Three kinds of buttons in alert dialogs
            // - Positive Buttpn
            // - Negative Button
            // - Neutral Button
        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Here the funtionality of the button can be defined
                finish();
            }
        });
        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // here the functionality of the button can be defined.
                dialogInterface.dismiss();
            }
        });
        alert.setNeutralButton("EXIT NOT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "I'm neutral!", Toast.LENGTH_SHORT).show();
                dialogInterface.dismiss();
            }
        });
        alert.setIcon(R.drawable.alert_icon);
        alert.show();
    }
}



   // TODO 1:  Add RecyclerView dependency to build.gradle if needed
   // TODO 2: Add RecyclerView to layout
   // TODO 3: Create XML layout for item
   // TODO 4: Extend RecyclerView.Adapter
   // TODO 5: Extend RecyclerView.ViewHolder
   // TODO 6: In Activity onCreate(), create RecyclerView with adapter and layout manager
