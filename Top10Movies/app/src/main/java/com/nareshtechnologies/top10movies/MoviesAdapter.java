package com.nareshtechnologies.top10movies;

import android.content.Context;
import android.graphics.Movie;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder> {

    // We are supposed to get the data from MainActivity
    Context context;
    private int[] moviePosters;
    private String[] movieNames, movieDirectors;
    // Creating a constructor that would initialize all the above variables.


    public MoviesAdapter(Context context, int[] moviePosters, String[] movieNames, String[] movieDirectors) {
        this.context = context;
        this.moviePosters = moviePosters;
        this.movieNames = movieNames;
        this.movieDirectors = movieDirectors;
    }

    /**
     * onCreateViewHolder - Returns a ViewHolder type Object
     * Responsibility of onCreateViewHolder is to inflate (Attach)
     * the layout design that we have to all the items in the recyclerview.
     */
    @NonNull
    @Override
    public MoviesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.one_row_design,parent,false);
        return new MoviesViewHolder(v);
    }

    /**
     * This method is responsible to populate the data based on the position of the item.
     */
    @Override
    public void onBindViewHolder(@NonNull MoviesViewHolder holder, int position) {
        holder.mPoster.setImageResource(moviePosters[position]);
        holder.mNames.setText(movieNames[position]);
        holder.mDirectors.setText(movieDirectors[position]);
    }

    /**
     * It tells the recyclerview adapter about how many items are there in total
     */
    @Override
    public int getItemCount() {
        return moviePosters.length;
    }

    /**
     * The MovieViewHolder (ViewHolder class of Recyclerview) will hold the view information
     * of the layout design file. Layout design file will be used as a template for all the
     * items in the recyclerview.
     */
    public class MoviesViewHolder extends RecyclerView.ViewHolder {
        ImageView mPoster;
        TextView mNames, mDirectors;
        public MoviesViewHolder(@NonNull View itemView) {
            super(itemView);
            mPoster = itemView.findViewById(R.id.movie_poster);
            mNames = itemView.findViewById(R.id.movie_name);
            mDirectors = itemView.findViewById(R.id.movie_director);
        }
    }
}
