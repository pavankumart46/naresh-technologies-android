package com.nareshittechnologies.viewmodel_livedata;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MainViewModel extends ViewModel {

    MutableLiveData<Integer> count = new MutableLiveData<>();
    public MainViewModel(){
        count.setValue(0);
        Log.v("MAINVIEWMODEL","Main View Model is created");
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.v("MAINVIEWMODEL","Main View Model is Destroyed");
    }

    public void increment(){
        count.setValue(count.getValue() + 1);
    }

    public void decrement(){
        count.setValue(count.getValue() - 1);
    }
}
