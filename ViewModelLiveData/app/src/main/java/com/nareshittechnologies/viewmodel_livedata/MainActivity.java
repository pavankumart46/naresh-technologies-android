package com.nareshittechnologies.viewmodel_livedata;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;

import com.nareshittechnologies.viewmodel_livedata.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    /*
    * LiveData helps us to observe the changes happening on a particular variable*/
    ActivityMainBinding binding;
    MainViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.activity_main);*/
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.count.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                binding.result.setText(String.valueOf(integer));
            }
        });
    }

    public void plusScore(View view) {
        viewModel.increment();
    }

    public void minusScore(View view) {
        viewModel.decrement();
    }
}