package com.nareshtechnologies.explicit_implicit_intents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent ty6 = getIntent();
        String message = ty6.getStringExtra("DATA");
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}