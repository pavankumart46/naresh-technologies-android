package com.nareshtechnologies.explicit_implicit_intents;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText d;
    private ActivityResultLauncher<Intent> s;
    private ActivityResultLauncher<Intent> contacts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        d = findViewById(R.id.data_here);
        s = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        Intent res = result.getData();
                        ImageView iv = findViewById(R.id.photo);
                        Bitmap b = res.getParcelableExtra("data");
                        iv.setImageBitmap(b);
                    }
                }
        );

        contacts = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        Intent data = result.getData();
                        Uri contactsData = data.getData();
                        Intent i = new Intent();
                        i.setAction(Intent.ACTION_VIEW);
                        i.setData(contactsData);
                        startActivity(i);
                    }
                }
        );

        /*Intent res = result.getData();
        ImageView iv = findViewById(R.id.photo);
        Bitmap b = res.getParcelableExtra("data");
        iv.setImageBitmap(b);*/
    }

    public void openNext(View view) {
        //Completed 1: Write intent code and take the user to the secondactivity
        // as we specify the exact activity to be opened up - This is an Explicit Intent
        String data = d.getText().toString();
        Intent i = new Intent(this,SecondActivity.class);
        i.putExtra("DATA",data);
        startActivity(i);
    }

    public void openCamera(View view) {
        Intent i = new Intent();
        i.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        s.launch(i);

    }

    public void pickContact(View view) {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_PICK);
        i.setType(ContactsContract.Contacts.CONTENT_TYPE);
        contacts.launch(i);
    }
}