package com.nareshtechnologies.hellotoast;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Step 1: Creating the objects for the views on activity_main
    private Button c, t;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Step2: Initialize the objects that are created in the first step
        // We have a method called 'findViewById()' using which we can initialize the objects
        c = findViewById(R.id.count);
        t = findViewById(R.id.toast);
        result = findViewById(R.id.value);

        // The clicks happening on the button has to be recognized.
        // we use setOnClickListener() method to recognize the clicks on button
        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "CLICKED", Toast.LENGTH_SHORT).show();
            }
        });

        c.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(MainActivity.this, "LONG CLICKED", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }
}