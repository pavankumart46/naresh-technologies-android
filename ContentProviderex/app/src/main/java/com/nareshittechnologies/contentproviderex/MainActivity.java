package com.nareshittechnologies.contentproviderex;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    // TODO 1: To Access the Content Provider, we need to have a Content URI.
    //  Inorder to let the android system understand that this is the content provider's
    //  URI, prefix it with "content://"
    String authorities = "content://com.nareshittechnologies.SqliteDatabase.Provider";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textview);

        // TODO 2: Convert the existing authorities to CONTENT_URI
        Uri CONTENT_URI = Uri.parse(authorities);

        // TODO 3: Pass the data request using ContentResolver
        Cursor c = getContentResolver().query(CONTENT_URI,null,null,null,null,null);

        // TODO 4: Parse the cursor data and display the results
        if(c!=null){
            Toast.makeText(this, "DAta has arrived", Toast.LENGTH_SHORT).show();
            textView.setText("");
            c.moveToFirst();
            do{
                int s_id = c.getInt(0);
                String s_n = c.getString(1);
                int s_a = c.getInt(2);
                textView.append(s_id+"\t"+s_n+"\t"+s_a+"\n");
            }while (c.moveToNext());
        }
    }
}