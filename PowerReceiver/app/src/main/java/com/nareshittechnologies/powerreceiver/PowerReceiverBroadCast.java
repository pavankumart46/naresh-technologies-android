package com.nareshittechnologies.powerreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.widget.ImageView;
import android.widget.Toast;

public class PowerReceiverBroadCast extends BroadcastReceiver {

    ImageView power_img, wifi_img;

    public PowerReceiverBroadCast(ImageView power_img, ImageView wifi_img) {
        this.power_img = power_img;
        this.wifi_img = wifi_img;
    }

    public PowerReceiverBroadCast() {
    }

    /*onReceive(...) method gets invoked as soon as the broadcast is
    * received by the application
    * - onReceive(...) is responsible to handle the broadcasts that are received.
    * - Whenever there is a new broadcast, you will get the broadcast wrapped inside an Intent object*/
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        switch (intent.getAction()){
            case Intent.ACTION_POWER_CONNECTED:
                // do something
                power_img.setImageResource(R.drawable.battery_in_charge);
                break;
            case Intent.ACTION_POWER_DISCONNECTED:
                // do someother thing
                power_img.setImageResource(R.drawable.battery_draining);
                break;
            case WifiManager.WIFI_STATE_CHANGED_ACTION:
                //do Something
                wifi_img.setImageResource(R.drawable.wifi_state_changes);
                break;

            case MainActivity.ACTION_CUSTOM_BROADCAST:
                Toast.makeText(context, "CUSTOM BROADCAST RECEIVED", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}