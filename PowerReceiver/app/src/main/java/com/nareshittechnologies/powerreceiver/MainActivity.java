package com.nareshittechnologies.powerreceiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView power_img, wifi_img;
    public static final String ACTION_CUSTOM_BROADCAST = "com.nareshittechnologies.powerreceiver.ACTION_CUSTOM";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        power_img = findViewById(R.id.power_img);
        wifi_img = findViewById(R.id.wifi_img);

        // TODO 1: Create an IntentFilter object and pass the Broadcasts that you want to register
        IntentFilter i = new IntentFilter();
        i.addAction(Intent.ACTION_POWER_CONNECTED);
        i.addAction(Intent.ACTION_POWER_DISCONNECTED);
        i.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        i.addAction(ACTION_CUSTOM_BROADCAST);

        // TODO 2: Register your broadcast using registerReceiver(...)
        registerReceiver(new PowerReceiverBroadCast(power_img,wifi_img),i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //TODO 3: Unregister your broadcast using unregisterReceiver(...)
        unregisterReceiver(new PowerReceiverBroadCast(power_img,wifi_img));
    }


    public void sendCustomBroadcast(View view) {
        // sendBroadcast(...) - Normal Broadcast
        Intent i = new Intent();
        i.setAction(ACTION_CUSTOM_BROADCAST); // setAction(...) normally allows any Strings.
        i.putExtra("INFO","This is My Msg");
        sendBroadcast(i);
    }
}