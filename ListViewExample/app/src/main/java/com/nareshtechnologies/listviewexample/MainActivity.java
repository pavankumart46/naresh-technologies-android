package com.nareshtechnologies.listviewexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView l = findViewById(R.id.list_view);

        String[] data = getResources().getStringArray(R.array.technologies);
        // The array adapter is responsible to properly populate the array of data in to the listview
        ArrayAdapter<String> a = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,data);
        l.setAdapter(a);

        l.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String tech = adapterView.getItemAtPosition(i).toString();
        Toast.makeText(this, tech, Toast.LENGTH_SHORT).show();
    }
}