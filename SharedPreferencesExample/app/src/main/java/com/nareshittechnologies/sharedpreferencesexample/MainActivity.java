package com.nareshittechnologies.sharedpreferencesexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    EditText et;
    TextView tv;
    // TODO 1: Create a Shared Preferences Obj
    SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et = findViewById(R.id.input_text);
        tv = findViewById(R.id.result);
        // TODO 2: Initialize the SharedPreferences file with the help of getSharedPreferences(..)
        preferences = getSharedPreferences("shared_prefs", MODE_PRIVATE);

        if(preferences!=null && preferences.contains("KEY")){
            loadData();
        }

        // TODO 4: register for on preference changes
        preferences.registerOnSharedPreferenceChangeListener(this);
    }

    public void saveData(View view) {
        String data = et.getText().toString();
        // TODO 3 : Save the data
            // - to save the data, we need SharedPreferences.Editor
            // - All the values that you would like to store should have a String KEY
            // - Without a Key, you cannot retrieve the data
            // - To save the changes call either commit(...) or apply(...)
            // - apply() is more thread friendly -try to use it more often.
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("KEY",data);
        editor.apply();

        et.getText().clear();
    }

    public void loadData() {
        String data = preferences.getString("KEY","SORRY DIDNOT FIND THE DATA");
        tv.setText(data);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        loadData();
    }

    public void clearData(View view) {
        SharedPreferences.Editor e = preferences.edit();
        e.clear();
        e.apply();
    }
}