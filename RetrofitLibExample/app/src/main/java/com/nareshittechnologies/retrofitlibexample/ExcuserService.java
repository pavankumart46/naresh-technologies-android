package com.nareshittechnologies.retrofitlibexample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ExcuserService {

    @GET("{category}/")
    Call<Excuse[]> getData(@Path("category") String c);
}
