package com.nareshittechnologies.retrofitlibexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class DetailedActivity extends AppCompatActivity {

    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);
        tv = findViewById(R.id.textView);
        Intent i = getIntent();
        Excuse e = (Excuse) i.getSerializableExtra("OBJECT");
        tv.setText(e.getExcuse());
    }
}