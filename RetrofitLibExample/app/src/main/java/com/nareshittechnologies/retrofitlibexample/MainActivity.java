package com.nareshittechnologies.retrofitlibexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MainActivity extends AppCompatActivity {

    String url = "https://excuser.herokuapp.com/v1/excuse/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.googleapis.com")
                .appendPath("books")
                .appendPath("v1")
                .appendPath("volumes")
                .appendQueryParameter("q", "quilting");
        Uri url = builder.build();
        Log.v("MAIN",url.toString());*/
    }

    public void fetchData(View view) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ExcuserService service = retrofit.create(ExcuserService.class);

        Call<Excuse[]> call = service.getData("children");

        call.enqueue(new Callback<Excuse[]>() {
            @Override
            public void onResponse(Call<Excuse[]> call, Response<Excuse[]> response) {
                Excuse[] e = response.body();
                Toast.makeText(MainActivity.this, e[0].getExcuse(), Toast.LENGTH_SHORT).show();
                ((TextView)findViewById(R.id.result)).setText(e[0].getExcuse());
                Intent i = new Intent(MainActivity.this,DetailedActivity.class);
                i.putExtra("OBJECT",e[0]);
                startActivity(i);
            }

            @Override
            public void onFailure(Call<Excuse[]> call, Throwable t) {

            }
        });
    }
}